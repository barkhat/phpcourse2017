<?php
include('userdata.php');
session_start();

if (isset($_POST['login']) && isset($_POST['password'])) {
    if (isset($users[$_POST['login']]) && $users[$_POST['login']]['password'] === $_POST['password']) {
        $_SESSION['login'] = array(
            'nickname' => $_POST['login'],
            'firstname' => $users[$_POST['login']]['firstname'],
            'lastname' => $users[$_POST['login']]['lastname'],
        );
        header('Location: auth_text.php');
    } else {
        echo 'Invalid credentials';
    }
}
?>
<html>
<head>
	<meta charset="UTF-8">
    <title>Аутентификация</title>
</head>
<body>
<h1>Войдите</h1>

<form method="POST" action="auth.php">
    <label>Логин:</label>
    <input type="text" name="login">

    <label>Пароль:</label>
    <input type="password" name="password">

    <button type="submit">Отправить</button>
</form>

</body>
</html>
