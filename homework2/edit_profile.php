<?php
session_start();
if (!isset($_SESSION['login'])) {
    header('Location: auth.php');
}
if (isset($_POST['firstname']) && isset($_POST['lastname'])) {
    $_SESSION['login']['firstname'] = $_POST['firstname'];
    $_SESSION['login']['lastname'] = $_POST['lastname'];
    header('Location: profile.php');
}
?>
<html>
<head>
	<meta charset="UTF-8">
    <title>Редактирование профиля</title>
</head>
<body>
<h1>Редактирование профиля</h1>
<form method="POST" action="edit_profile.php">
    <label for="firsname">Имя</label>
    <input id="firstname" type="text" name="firstname" value="<?php echo $_SESSION['login']['firstname']; ?>">
    <label for="lastname">Фамилия</label>
    <input id="lastname" type="text" name="lastname" value="<?php echo $_SESSION['login']['lastname']; ?>">
    <button type="submit">Сохранить</button>
</form>
<a href="profile.php">Профиль пользователя</a>
<a href="logout.php">Выйти</a>
</body>
</html>