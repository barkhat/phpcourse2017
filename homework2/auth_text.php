<?php
session_start();
if (!isset($_SESSION['login'])) {
    header('Location: auth.php');
}
?>
<html>
<head>
	<meta charset="UTF-8">
    <title>Текст только для авторизованных пользователей</title>
</head>
<body>
<h1>Текст только для авторизованных пользователей</h1>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet erat eget sapien sollicitudin porta mattis sit amet nisi. Vivamus ultricies dui id ante commodo, quis suscipit nunc ullamcorper. Praesent a sem ante. Praesent id pretium felis. Ut laoreet felis lacus, non malesuada nulla egestas a. Maecenas leo leo, interdum sit amet porta et, volutpat eu urna. Nam vulputate augue ac mi pulvinar, vel rhoncus enim vehicula. Pellentesque vestibulum iaculis hendrerit. Sed in lorem sem. Praesent malesuada sapien ex, a faucibus lacus venenatis ut. Phasellus dictum turpis pharetra lacus convallis, vitae congue dolor commodo. Cras sagittis varius dapibus. Morbi fermentum urna a egestas tincidunt. Etiam eget neque ut ipsum rutrum accumsan vitae eget magna.</p>
<a href="profile.php">Профиль пользователя</a>
<a href="logout.php">Выйти</a>
</body>
</html>