<?php
session_start();
if (!isset($_SESSION['login'])) {
    header('Location: auth.php');
}
?>
<html>
<head>
	<meta charset="UTF-8">
    <title>Профиль пользователя</title>
</head>
<body>
<h1>Профиль пользователя</h1>
<p>Ваш логин: <?php echo $_SESSION['login']['nickname']; ?></p>
<p>Ваше имя: <?php echo $_SESSION['login']['firstname']; ?></p>
<p>Ваша фамилия: <?php echo $_SESSION['login']['lastname']; ?></p>

<p><a href="edit_profile.php">Редактировать профиль</a></p>
<p><a href="auth_text.php">На главную</a><p>
<p><a href="logout.php">Выйти</a></p>

</body>
</html>
